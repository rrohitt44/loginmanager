<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Registration</title>
</head>
<body> 
<c:if test="${not empty errorMessage}"><div style="color:red; font-weight: bold; margin: 30px 0px;">${errorMessage}</div></c:if>

<form:form id="regForm" modelAttribute="user" action="register" method="post">
<form:errors /> 
<table align="center">
   <tr>
       <td>
           <form:label path="username">Username</form:label>
       </td>
       <td>
           <form:input path="username" name="username" id="username" />
           <form:errors path="username"></form:errors>
       </td>
   </tr>
   <tr>
       <td>
           <form:label path="password">Password</form:label>
       </td>
       <td>
           <form:password path="password" name="password" id="password" />
           <form:errors path="password"></form:errors>
       </td>
   </tr>
   <tr>
       <td>
           <form:label path="matchingPassword">Confirm Password</form:label>
       </td>
       <td>
           <form:password path="matchingPassword" name="matchingPassword" id="matchingPassword" />
      <form:errors path="matchingPassword"></form:errors>
       </td>
   </tr>
   <tr>
       <td>
           <form:label path="email">Email</form:label>
       </td>
       <td>
           <form:input path="email" name="email" id="email" />
            <form:errors path="email"/>
       </td>
   </tr>
   <tr>
       <td></td>
       <td>
           <form:button id="register" name="register">Register</form:button>
        </td>
    </tr>
    <tr></tr>
    <tr>
        <td><a href="logout">Click here to Logout</a></td>
        <td><a href="home">Home</a></td>
        
    </tr>
</table>
</form:form>
</body>
</html>   