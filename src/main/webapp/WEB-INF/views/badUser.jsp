<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
var serverContext = "http://localhost:8080/";

function resendToken(){
	var token = '${token}';
	alert("token resending: "+serverContext + "resendConfirmationToken?token="+token);
	$.get(serverContext + "resendConfirmationToken?token="+token,
			function(data){
			window.location.href=serverContext+"login?message="+data.message;
		})
		.fail(
				function(data){
					if(data.responseJSON.error.indexOf('MailError') > -1){
						window.location.href=serverContext+"emailError.html";
						}
					else{
						window.location.href=serverContext+"login?message="+data.responseJSON.message;
						}
					}
				)
		;
		
}
</script>
</head>
<body>
<c:if test="${not empty message}"><div style="color:green; font-weight: bold; margin: 30px 0px;">${message}</div></c:if>
<c:if test="${fn:contains(message, 'Invalid') }">
<%@include file="index.jsp" %>
</c:if>
<c:if test="${expired eq true}">
<button onclick="resendToken()">resent Token</button>
<%@include file="index.jsp" %>
</c:if>
</body>
</html>