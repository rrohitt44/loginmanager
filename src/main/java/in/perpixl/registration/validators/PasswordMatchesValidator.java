package in.perpixl.registration.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import in.perpixl.registration.annotations.validators.PasswordMatches;
import in.perpixl.registration.dto.UserDto;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object>
{

	private String message;
	@Override
	public void initialize(PasswordMatches constraintAnnotation)
	{
		ConstraintValidator.super.initialize(constraintAnnotation);
		this.message = constraintAnnotation.message();
	}
	
	@Override
	public boolean isValid(Object obj, ConstraintValidatorContext context)
	{
		UserDto dto = (UserDto) obj;
		boolean isValid = dto.getPassword().equals(dto.getMatchingPassword());

        if (!isValid) {
            context.disableDefaultConstraintViolation();
            context
                    .buildConstraintViolationWithTemplate( message )
                    .addPropertyNode( "matchingPassword" ).addConstraintViolation();
        }

        return isValid;
        
	}

}
