package in.perpixl.registration.listener;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import in.perpixl.registration.repository.PrivilegeRepository;
import in.perpixl.registration.repository.RoleRepository;
import in.perpixl.registration.repository.UserRepository;
import in.perpixl.registration.repository.entity.Privilege;
import in.perpixl.registration.repository.entity.Role;
import in.perpixl.registration.repository.entity.User;

@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent>
{

	boolean alreadySetup = false;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private RoleRepository roleRepo;
	
	@Autowired
	private PrivilegeRepository privilegeRepo;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	@Transactional
	public void onApplicationEvent(ContextRefreshedEvent event)
	{
		if(alreadySetup)
			return;
		
		Privilege readPrivilege = createPrivilegeIfNotFound("READ_PRIVILEGE");
		Privilege writePrivilege = createPrivilegeIfNotFound("WRITE_PRIVILEGE");
		
		List<Privilege> adminPrivileges = Arrays.asList(readPrivilege, writePrivilege);
		
		createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
		createRoleIfNotFound("ROLE_USER", Arrays.asList(readPrivilege));
		
		Role adminRole = roleRepo.findByName("ROLE_ADMIN");
		Role userRole = roleRepo.findByName("ROLE_USER");
		
		User adminUser = new User();
		adminUser.setUsername("admin");
		adminUser.setPassword(passwordEncoder.encode("admin"));
		adminUser.setEmail("rohit.muneshwar@ymail.com");
		adminUser.setRoles(Arrays.asList(adminRole));
		adminUser.setEnabled(true);
		userRepo.save(adminUser);
		
		User localUser = new User();
		localUser.setUsername("user");
		localUser.setPassword(passwordEncoder.encode("user"));
		localUser.setEmail("rrohitt.muneshwar@gmail.com");
		localUser.setRoles(Arrays.asList(userRole));
		localUser.setEnabled(true);
		userRepo.save(localUser);
		
		alreadySetup = true;
	}

	@Transactional
	private Role createRoleIfNotFound(String roleName, Collection<Privilege> privileges)
	{
		Role role = roleRepo.findByName(roleName);
		if(role == null) {
			role = new Role();
			role.setName(roleName);
			role.setPrivileges(privileges);
			roleRepo.save(role);
		}
		return role;
	}

	@Transactional
	private Privilege createPrivilegeIfNotFound(String privName)
	{
		Privilege privilege = privilegeRepo.findByName(privName);
		if(privilege==null) {
			privilege = new Privilege();
			privilege.setName(privName);
			privilegeRepo.save(privilege);
		}
		return privilege;
	}

}
