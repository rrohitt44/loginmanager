package in.perpixl.registration.listener;

import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.context.event.EventListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import in.perpixl.registration.event.OnRegistrationCompleteEvent;
import in.perpixl.registration.repository.entity.User;
import in.perpixl.registration.service.MyUserDetailsService;
import in.perpixl.registration.service.UserService;

/**
 * @author RohitMuneshwar
 * 
 * There are a few simple guidelines to follow for events:

		the event should extend ApplicationEvent
		the publisher should inject an ApplicationEventPublisher object (Alternatively, the publisher class can implement the ApplicationEventPublisherAware)
		the listener should implement the ApplicationListener interface
		
	Spring allows to create and publish custom events which – by default – are synchronous.
 *
 */
@Component
public class RegistrationListener// implements ApplicationListener<OnRegistrationCompleteEvent>
{
	@Autowired
	MyUserDetailsService service;
	
	@Autowired
	UserService userService;
	
	@Autowired
	MessageSource messages;
	
	@Autowired
	JavaMailSender mailSender;
	
	//@Override
	@EventListener
	public void onApplicationEvent(OnRegistrationCompleteEvent event)
	{
		this.confirmRegistration(event);
	}
	
	private void confirmRegistration(OnRegistrationCompleteEvent event) {
		System.out.println("Comfirming registration");
		User user = event.getRegistered();
		String token = UUID.randomUUID().toString();
		System.out.println("User service type - "+userService);
		userService.createVerificationToken(user, token);
		System.out.println("Verification Token Created");
		// prepare mail data
		String recipientAddress = user.getEmail();
		String subject = "Registration Confirmation";
		String confirmationUrl = event.getAppUrl()+"/registrationConfirm?token="+token;
		String message = messages.getMessage("message.regSucc", null, event.getLocale());
		System.out.println("Message prepared");
		// send mail
		SimpleMailMessage email = new SimpleMailMessage();
		email.setTo(recipientAddress);
		email.setSubject(subject);
		email.setText(message+"\r\n"+"http://localhost:8080"+confirmationUrl);
		System.out.println("Sending mail");
		mailSender.send(email);
		System.out.println("Mail sent");
	}

}
