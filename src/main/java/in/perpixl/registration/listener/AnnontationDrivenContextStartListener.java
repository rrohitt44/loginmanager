package in.perpixl.registration.listener;

import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.ContextStoppedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class AnnontationDrivenContextStartListener
{
	/**
	 * @param event
	 * 
	 * As before, the method signature declares the event type it consumes. 
	 * As before, this listener is invoked synchronously. But now making it asynchronous is 
	 * as simple as adding an @Async annotation
	 */
	//@Async
	@EventListener
	public void handleContextStart(ContextRefreshedEvent event) {
		System.out.println("Handling context refreshed event synchronously -"+ Thread.currentThread().getName());
	}
	
	@Async
	@EventListener
	public void handleContextStop(ContextRefreshedEvent event) {
		System.out.println("Handling context refreshed event asynchronously-"+ Thread.currentThread().getName());
	}
	
	@EventListener(classes = { ContextStartedEvent.class, ContextRefreshedEvent.class })
	public void handleMultipleEvents() {
	    System.out.println("Multi-event listener invoked synchronously-"+ Thread.currentThread().getName());
	}
	
	@Async("threadPoolTaskExecutor")
	public void asyncMethodWithConfiguredExecutor() {
	    System.out.println("Execute method with configured executor - "
	      + Thread.currentThread().getName());
	}
}
