package in.perpixl.registration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import in.perpixl.registration.listener.AnnontationDrivenContextStartListener;

@SpringBootApplication
public class RegistrationApplication implements CommandLineRunner{

	@Autowired
	AnnontationDrivenContextStartListener asyncExecute;
	
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(RegistrationApplication.class);
		app.run(args);
	}

	@Override
	public void run(String... args) throws Exception
	{
		asyncExecute.asyncMethodWithConfiguredExecutor();
		/*
		 * AnnotationConfigApplicationContext context = new
		 * AnnotationConfigApplicationContext(RegistrationApplication.class);
		 * context.start(); context.stop(); context.close();
		 */
	}

}
