package in.perpixl.registration;

import java.util.concurrent.Executor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ApplicationEventMulticaster;
import org.springframework.context.event.SimpleApplicationEventMulticaster;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * @author RohitMuneshwar
 * 
 * You can turn asynchronous behavior on in the configuration by creating 
 * an ApplicationEventMulticaster bean with an executor.
 * 
 * The event, the publisher and the listener implementations remain the same as before like for sync events
 * – but now, the listener will asynchronously deal with the event in a separate thread.
 * 
 * Starting with Spring 4.2, an event listener is not required to be a bean implementing 
 * the ApplicationListener interface – it can be registered on any public method of a managed bean 
 * via the @EventListener annotation:
 * 
 * annotation – by default, @EnableAsync detects Spring's @Async annotation and the EJB 3.1 
 * javax.ejb.Asynchronous; this option can be used to detect other, user-defined annotation 
 * types as well
 *
 */
@Configuration
@EnableAsync
public class AsynchronousSpringEventsConfig
{
	/**
	 * @return
	 */
	@Bean(name="applicationEventMulticaster")
	public ApplicationEventMulticaster simpleApplicationEventMulticaster() {
		SimpleApplicationEventMulticaster eventMulticaster = new SimpleApplicationEventMulticaster();
		eventMulticaster.setTaskExecutor(new SimpleAsyncTaskExecutor());
		return eventMulticaster;
	}
	
	@Bean(name = "threadPoolTaskExecutor")
    public Executor threadPoolTaskExecutor() {
        return new ThreadPoolTaskExecutor();
    }
}
