package in.perpixl.registration;

import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
public class MailConfig
{
	@Bean
	public JavaMailSender javaMailService() {
		JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
		//javaMailSender.setHost("smtp.gmail.com");
		//Try ports 110 and 143, 517
        //javaMailSender.setPort(517);
        javaMailSender.setUsername("rrohitt.muneshwar@gmail.com");
        javaMailSender.setPassword("_Shah2adaKing");
        javaMailSender.setJavaMailProperties(getMailProperties());

        return javaMailSender;
	}
	
	private Properties getMailProperties() {
        Properties props = new Properties();
		/*
		 * properties.setProperty("mail.transport.protocol", "smtp");
		 * properties.setProperty("mail.smtp.auth", "false");
		 * properties.setProperty("mail.smtp.starttls.enable", "false");
		 * 
		 */
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        //props.put("mail.smtp.user", "rrohitt.muneshwar@gmail.com");
        //props.put("mail.smtp.password", "_Shah2adaKing");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.setProperty("mail.debug", "true");
        return props;
    }
}
