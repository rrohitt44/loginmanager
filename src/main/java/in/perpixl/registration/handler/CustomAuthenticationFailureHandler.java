package in.perpixl.registration.handler;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.LocaleResolver;

@Component
public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler
{

	@Autowired
	private MessageSource messages;

	@Autowired
	LocaleResolver localResolver;

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException
	{
		System.out.println("AuthenticationException occured");
		setDefaultFailureUrl("/login?error=true");
		super.onAuthenticationFailure(request, response, exception);
		Locale locale = localResolver.resolveLocale(request);
		String errorMessage = messages.getMessage("message.badCredentials", null, locale);
		if (exception.getMessage().equalsIgnoreCase("User is disabled"))
		{
			errorMessage = messages.getMessage("auth.message.disabled", null, locale);
		} else if (exception.getMessage().equalsIgnoreCase("User account has expired"))
		{
			errorMessage = messages.getMessage("auth.message.expired", null, locale);
		}
		System.out.println("AuthenticationException occured: errorMessage - "+errorMessage);
		request.getSession().setAttribute(WebAttributes.AUTHENTICATION_EXCEPTION, errorMessage);
	}
}
