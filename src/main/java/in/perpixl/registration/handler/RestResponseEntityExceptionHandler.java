package in.perpixl.registration.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import in.perpixl.registration.dto.GenericResponse;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler
{

	@Autowired
	MessageSource messages;

	@ExceptionHandler(
	{ UsernameNotFoundException.class })
	public ResponseEntity<Object> handleUserNotFound(RuntimeException ex, WebRequest request)
	{
		System.out.println("UsernameNotFoundException occured");
		GenericResponse bodyOfResponse = new GenericResponse(
				messages.getMessage("message.userNotFound", null, request.getLocale()), "UserNotFound");

		return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
	}

	@ExceptionHandler(
	{ MailAuthenticationException.class })
	public ResponseEntity<Object> handleMail(RuntimeException ex, WebRequest request)
	{
		System.out.println("MailAuthenticationException occured");
		GenericResponse bodyOfResponse = new GenericResponse(
				messages.getMessage("message.email.config.error", null, request.getLocale()), "MailError");

		return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
	}
	
	public ResponseEntity<Object> handleUserAlreadyExists(RuntimeException ex, WebRequest request){
		System.out.println("409 status code");
		GenericResponse bodyOfResponse = new GenericResponse(
				messages.getMessage("message.regError", null, request.getLocale()),
				"UserAlreadyExist"
				);
		return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.CONFLICT, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status,
			WebRequest request)
	{
		System.out.println("404 status code");
		BindingResult result = ex.getBindingResult();
		GenericResponse bodyOfResponse = new GenericResponse(result.getFieldErrors(), result.getGlobalErrors());
		return handleExceptionInternal(ex,bodyOfResponse,new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}

	@ExceptionHandler(
	{ Exception.class })
	public ResponseEntity<Object> handleInternal(RuntimeException ex, WebRequest request)
	{
		System.out.println("Exception occured");
		GenericResponse bodyOfResponse = new GenericResponse(
				messages.getMessage("message.error", null, request.getLocale()), "InternalError");

		return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
	}
}
