package in.perpixl.registration.handler;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

public class MyAuthenticationSuccessHandler implements AuthenticationSuccessHandler
{

	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException
	{
		handle(request, response, authentication);
		clearAuthenticationAttributes(request);
	}

	private void clearAuthenticationAttributes(HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		if(session == null) {
			return;
		}
		session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	}

	protected void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException
	{
		String targetUrl = determineTargetUrl(authentication);
		if(response.isCommitted()) {
			System.out.println("Response has already been committed. Unable to redirect to "
					+ targetUrl);
			return;
		}
		redirectStrategy.sendRedirect(request, response, targetUrl);
	}

	private String determineTargetUrl(Authentication authentication)
	{
		Map<String, String> roleTargetUrlMap = new HashMap<>();
	    roleTargetUrlMap.put("ROLE_USER", "/home");
	    roleTargetUrlMap.put("ROLE_ADMIN", "/adminhome");
	    roleTargetUrlMap.put("READ_PRIVILEGE", "/home");
	    roleTargetUrlMap.put("WRITE_PRIVILEGE", "/adminhome");
	    
	    final Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
	    String target = null;
	    for (final GrantedAuthority grantedAuthority : authorities) {
	        String authorityName = grantedAuthority.getAuthority();
	        System.out.println("authorityName - "+authorityName);
	        if(roleTargetUrlMap.containsKey(authorityName)) {
	        	target = roleTargetUrlMap.get(authorityName);
	        }
	    }
	    
	    if(target!=null) {
	    	return target;
	    }else {
	    	throw new IllegalStateException();
	    }
	}

}
