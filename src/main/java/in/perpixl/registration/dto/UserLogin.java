package in.perpixl.registration.dto;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import in.perpixl.registration.annotations.validators.PasswordMatches;
import in.perpixl.registration.annotations.validators.ValidEmail;

public class UserLogin
{
	@NotNull
	@NotEmpty
	private String username;
	
	
	@NotNull
	@NotEmpty
	private String password;
	
	
	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public UserLogin()
	{
		super();
	}

	

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public List<String> getRoles()
	{
		return null;
	}
	
}
