package in.perpixl.registration.exceptions;

public class UserAlreadyExistsException extends RuntimeException
{
	public UserAlreadyExistsException(String message)
	{
		super(message);
	}
}
