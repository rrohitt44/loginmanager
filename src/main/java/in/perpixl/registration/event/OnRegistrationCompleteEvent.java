package in.perpixl.registration.event;

import java.util.Locale;

import org.springframework.context.ApplicationEvent;
import org.springframework.stereotype.Component;

import in.perpixl.registration.repository.entity.User;

public class OnRegistrationCompleteEvent extends ApplicationEvent
{
	private User registered;
	private Locale locale;
	private String appUrl;
	public OnRegistrationCompleteEvent(User registered, 
			Locale locale, String appUrl)
	{
		super(registered);
		this.registered = registered;
		this.locale = locale;
		this.appUrl = appUrl;
	}
	public User getRegistered()
	{
		return registered;
	}
	public void setRegistered(User registered)
	{
		this.registered = registered;
	}
	public Locale getLocale()
	{
		return locale;
	}
	public void setLocale(Locale locale)
	{
		this.locale = locale;
	}
	public String getAppUrl()
	{
		return appUrl;
	}
	public void setAppUrl(String appUrl)
	{
		this.appUrl = appUrl;
	}
	
	

}
