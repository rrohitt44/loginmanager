package in.perpixl.registration.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import in.perpixl.registration.dto.GenericResponse;
import in.perpixl.registration.dto.UserDto;
import in.perpixl.registration.event.OnRegistrationCompleteEvent;
import in.perpixl.registration.repository.entity.User;
import in.perpixl.registration.service.UserService;

@Controller
public class RegWsController
{
	@Autowired
	ApplicationEventPublisher eventPublisher;

	@Autowired
	JavaMailSender mailSender;

	@Autowired
	UserService userService;

	// Rest service type

	@PostMapping("/register-ws")
	@ResponseBody
	public GenericResponse registerUserAccountRestfulWay(@Valid UserDto userDto, HttpServletRequest request)
	{
		System.out.println("Registering with information: " + userDto);
		// throws UserAlreadyExistsException is user is already present
		User registered = userService.registerNewUserAccount(userDto);

		String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();

		System.out.println("Publishing Event Registration complete");
		System.out.println("Event Publisher - " + eventPublisher.getClass().getName());
		eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, request.getLocale(), appUrl));
		System.out.println("Event triggered");
		System.out.println("User " + userDto.getUsername() + " is registered successfully.");

		return new GenericResponse("success");
	}
	
	@PreAuthorize("hasAuthority('WRITE_PRIVILEGE')")
	@RequestMapping(value = "/user")
    public String user(Model model, Principal principal) {
         
        UserDetails currentUser 
          = (UserDetails) ((Authentication) principal).getPrincipal();
        model.addAttribute("username", currentUser.getUsername());
        return "home";
    }

}
