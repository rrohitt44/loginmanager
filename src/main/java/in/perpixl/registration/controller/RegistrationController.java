package in.perpixl.registration.controller;

import java.util.Calendar;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

import in.perpixl.registration.dto.GenericResponse;
import in.perpixl.registration.dto.UserDto;
import in.perpixl.registration.event.OnRegistrationCompleteEvent;
import in.perpixl.registration.exceptions.UserAlreadyExistsException;
import in.perpixl.registration.repository.entity.User;
import in.perpixl.registration.repository.entity.VerificationToken;
import in.perpixl.registration.service.MyUserDetailsService;
import in.perpixl.registration.service.UserService;

@Controller
public class RegistrationController
{
	@Autowired
	UserService userService;
	
	@Autowired
	MyUserDetailsService myUserService;
	
	@Autowired
	MessageSource messages;
	
	@Autowired
	ApplicationEventPublisher eventPublisher;
	
	@Autowired
	JavaMailSender mailSender;
	
	
	  @GetMapping("/") public String index() { return "index"; }
	 
	
	@GetMapping("/register")
	public String showRegistrationForm(WebRequest request, Model model) {
		UserDto dto = new UserDto();
		model.addAttribute("user", dto);
		return "registration";
	}
	
	// Spring MVC type
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String registerUserAccount(@ModelAttribute("user") @Valid UserDto userDto, Errors errors,
			HttpServletRequest request, Model model)
	{

		if (errors.hasErrors())
		{
			return "registration";
		}

		try
		{
			User registered = userService.registerNewUserAccount(userDto);
			model.addAttribute("username", registered.getUsername());
			String appUrl = request.getContextPath();
			System.out.println("Publishing Event Registration complete");
			System.out.println("Event Publisher - " + eventPublisher.getClass().getName());
			eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, request.getLocale(), appUrl));
			System.out.println("Event triggered");
		} catch (UserAlreadyExistsException uaeEx)
		{
			model.addAttribute("errorMessage", uaeEx.getMessage());
			System.out.println("UserAlreadyExistsException occured " + uaeEx.getMessage());
			return "registration";
		} catch (RuntimeException ex)
		{
			model.addAttribute("errorMessage", "RuntimeException Occurred");
			System.out.println("RuntimeException occured " + ex.getMessage());
			return "registration";
		}
		model.addAttribute("message", "User " + userDto.getUsername() + " is registered successfully. Go to "
				+ userDto.getEmail()
				+ " to activate your account. If already activated then you can login using your account credentials.");
		System.out.println("User " + userDto.getUsername() + " is registered successfully.");
		return "login";
	}
	 
	
	@RequestMapping(value="/login", method = {RequestMethod.GET, RequestMethod.POST})
	public String login(@RequestParam(value="error", required=false) String error,
			@RequestParam(value="logout", required=false) String logout,
			@RequestParam(value="message", required=false) String message,
			ModelMap model) {
		String errorMessage= null;
		if(error != null) {
			errorMessage = "Username or Password is incorrect !!";
        }
        if(logout != null) {
        	errorMessage = "You have been successfully logged out !!";
        }
        model.addAttribute("message", message);
        model.addAttribute("errorMessage", errorMessage);
		return "login";
	}
	
	@RequestMapping(value="/home", method = {RequestMethod.GET, RequestMethod.POST})
	public String homePage(Model model) {
		model.addAttribute("username", "there");
		return "home";
	}
	
	@RequestMapping(value="/adminhome", method = {RequestMethod.GET, RequestMethod.POST})
	public String adminhomePage(Model model) {
		model.addAttribute("username", "admin");
		return "adminhome";
	}
	
	@RequestMapping(value="/badUser", method = {RequestMethod.GET, RequestMethod.POST})
	public String badUserPage(@RequestParam(value="message", required=false) String message,
			@RequestParam(value="expired", required=false) boolean expired,
			@RequestParam(value="token", required=false) String token,
			Model model) {
		model.addAttribute("message", message);
		model.addAttribute("expired", expired);
		model.addAttribute("token", token);
		return "badUser";
	}
	
	@RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
		/*
		 * Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		 * if (auth != null){ new SecurityContextLogoutHandler().logout(request,
		 * response, auth); }
		 */
        return "redirect:/login?logout=true";
    }
	
	@GetMapping("/registrationConfirm")
	public String confirmRegistration(WebRequest request, Model model, @RequestParam("token") String token) {
		Locale locale = request.getLocale();
		System.out.println("registration confirmation in progress");
		VerificationToken  verificationToken = userService.getVerificationToken(token);
		if(verificationToken == null) {
			System.out.println("Verification token is null");
			String message = messages.getMessage("auth.message.invalidToken", null, locale);
			model.addAttribute("message", message);
			return "redirect:/badUser?lang="+locale.getLanguage();
		}
		
		User user = verificationToken.getUser();
		System.out.println("User details - "+user);
		Calendar cal =Calendar.getInstance();	
		if((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
			System.out.println("Registration token expired");
			String message = messages.getMessage("auth.message.expired", null, locale);
			model.addAttribute("message", message);
			model.addAttribute("expired",true);
			model.addAttribute("token", token);
			return "redirect:/badUser?lang="+locale.getLanguage();
		}
		user.setEnabled(true);
		userService.saveRegistered(user);
		model.addAttribute("message", messages.getMessage("message.accountVerified", null, locale));
		System.out.println("Account verified successfully");
		return "redirect:/login?lang="+request.getLocale().getLanguage();
	}
	
	@GetMapping("/resendConfirmationToken")
	public GenericResponse resendRegistrationToken(HttpServletRequest request, @RequestParam("token") String existingToken) {
		VerificationToken newToken = userService.generateNewVerificationToken(existingToken);
		User user = userService.getUser(newToken.getToken());
		String appUrl = "http://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath();
		SimpleMailMessage email = constructResentVerificationTokenMail(appUrl, request.getLocale(), newToken, user);
		mailSender.send(email);
		return new GenericResponse(
			      messages.getMessage("message.resendToken", null, request.getLocale()));
	}

	private SimpleMailMessage constructResentVerificationTokenMail(String appUrl, Locale locale,
			VerificationToken newToken, User user)
	{
		String confirmationUrl = appUrl + "/registrationConfirm?token="+newToken.getToken();
		String message = messages.getMessage("message.resendToken", null, locale);
		SimpleMailMessage email = new SimpleMailMessage();
		email.setSubject("Resent registration token");
		email.setText(message+"\r\n"+confirmationUrl);
		//TODO - env.getProperty("support.email")
		email.setFrom("support.email.com");
		email.setTo(user.getEmail());
		return email;
	}
}
