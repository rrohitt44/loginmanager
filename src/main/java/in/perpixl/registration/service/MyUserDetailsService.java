package in.perpixl.registration.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import in.perpixl.registration.dto.UserLogin;
import in.perpixl.registration.exceptions.UserNotExistsException;
import in.perpixl.registration.repository.RoleRepository;
import in.perpixl.registration.repository.UserRepository;
import in.perpixl.registration.repository.entity.Privilege;
import in.perpixl.registration.repository.entity.Role;
import in.perpixl.registration.repository.entity.User;

@Service//("userDetailsService")
@Transactional
public class MyUserDetailsService implements UserDetailsService
{
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private RoleRepository roleRepo;
	
	public User validateUserLogin(UserLogin userDto) throws UserNotExistsException
	{
		User user = userRepo.findByUsername(userDto.getUsername());
		if(user == null) {
			throw new UserNotExistsException("user "+userDto.getUsername()+" is currently not registered. Please register now.");
		}
		
		System.out.println("user - "+user);
		return user;
	}
	
	private static List<GrantedAuthority> getAuthorities(Collection<Role> roles)
	{
		List<GrantedAuthority> authorities = getGrantedAuthorities(getPrivilege(roles));
		return authorities;
	}

	private static List<GrantedAuthority> getGrantedAuthorities(List<String> privileges)
	{
		List<GrantedAuthority> authorities = new ArrayList<>();
		for(String privilege: privileges) {
			System.out.println("Privileges: "+privilege);
			authorities.add(new SimpleGrantedAuthority(privilege));
		}
		return authorities;
	}

	private static List<String> getPrivilege(Collection<Role> roles)
	{
		List<String> privileges = new ArrayList<>();
		List<Privilege> collection = new ArrayList<>();
		
		for(Role role:roles) {
			System.out.println("Role: "+role.getName());
			collection.addAll(role.getPrivileges());
		}
		
		for(Privilege priv: collection) {
			privileges.add(priv.getName());
		}
		return privileges;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
	{
		UserDetails userDetails = null;
		try
		{
			User user = userRepo.findByUsername(username);
			System.out.println("loadUserByUsername: " + user);
			// authorize
			boolean enabled = true;
			boolean accountNotExpired = true;
			boolean credentialsNotExpired = true;
			boolean accountNotLocked = true;
			if (user == null)
			{
				// throw new UsernameNotFoundException("user "+username+" is currently not
				// registered. Please register now.");
				userDetails = new org.springframework.security.core.userdetails.User(" ", " ", enabled,
						accountNotExpired, credentialsNotExpired, accountNotLocked,
						getAuthorities(Arrays.asList(roleRepo.findByName("ROLE_USER"))));
			} else
			{
				enabled = user.isEnabled();
				userDetails = new org.springframework.security.core.userdetails.User(username,
						user.getPassword(), enabled, accountNotExpired, credentialsNotExpired,
						accountNotLocked, getAuthorities(user.getRoles()));
			}
		} catch (Exception e)
		{
			System.out.println("Exception occurred - "+e.getMessage());
			throw new RuntimeException(e);
		}
		return userDetails;
	}
}
