package in.perpixl.registration.service;

import java.util.Arrays;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import in.perpixl.registration.dto.UserDto;
import in.perpixl.registration.exceptions.UserAlreadyExistsException;
import in.perpixl.registration.repository.RoleRepository;
import in.perpixl.registration.repository.UserRepository;
import in.perpixl.registration.repository.VerificationTokenRepository;
import in.perpixl.registration.repository.entity.User;
import in.perpixl.registration.repository.entity.VerificationToken;

@Service
public class UserService
{
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	VerificationTokenRepository tokenRepo;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	RoleRepository roleRepo;
	
	@Transactional
	public User registerNewUserAccount(UserDto userDto) throws UserAlreadyExistsException
	{
		if(userExists(userDto.getUsername())) {
			throw new UserAlreadyExistsException(
		              "There is an account with that email address: "
		              +  userDto.getUsername());
		}
		User user = new User();    
        user.setUsername(userDto.getUsername());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setEmail(userDto.getEmail());
        user.setRoles(Arrays.asList(roleRepo.findByName("ROLE_USER")));
        return userRepo.save(user);
	}

	private boolean emailExists(String email)
	{
		return userRepo.findByEmail(email) != null;
	}

	private boolean userExists(String username)
	{
		return userRepo.findByUsername(username) != null;
	}
	
	public User getUser(String verificationToken) {
		return tokenRepo.findByToken(verificationToken).getUser();
	}
	public VerificationToken getVerificationToken(String token)
	{
		return tokenRepo.findByToken(token);
	}

	public void saveRegistered(User user)
	{
		userRepo.save(user);
	}
	
	public void createVerificationToken(User user, String token)
	{
		VerificationToken vtoken = new VerificationToken();
		vtoken.setToken(token);
		vtoken.setUser(user);
		tokenRepo.save(vtoken);
		
	}

	public VerificationToken generateNewVerificationToken(String existingToken)
	{
		return tokenRepo.findByToken(existingToken);
	}
	
}
