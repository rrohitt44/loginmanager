package in.perpixl.registration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.kerberos.authentication.KerberosAuthenticationProvider;
import org.springframework.security.kerberos.authentication.KerberosServiceAuthenticationProvider;
import org.springframework.security.kerberos.authentication.sun.SunJaasKerberosClient;
import org.springframework.security.kerberos.authentication.sun.SunJaasKerberosTicketValidator;
import org.springframework.security.kerberos.web.authentication.SpnegoAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import in.perpixl.registration.handler.MyAuthenticationSuccessHandler;
import in.perpixl.registration.service.MyUserDetailsService;

@Configuration
@EnableWebSecurity // get spring security and mvc support
@EnableGlobalMethodSecurity(prePostEnabled=true) // with this we can annotate our resources with @PreAuthorize and @PostAuthorize for fine-grained access control
public class SecurityConfig extends WebSecurityConfigurerAdapter
{
	@Autowired
	private MyUserDetailsService userService;
	
	@Value("${app.keytab-location}")
    private String kerberosKeytabConfPath;
	
	@Value("${app.kerberos-principle}")
    private String kerberosPrinciple;
	
	
	// used to secure different URLs that need security
	@Override
	protected void configure(HttpSecurity http) throws Exception
	{
		http.authorizeRequests()
		.antMatchers("/h2-console/**").permitAll()
	  .antMatchers("/home").access("hasAuthority('READ_PRIVILEGE')")
	  .antMatchers("/register/**").permitAll() .antMatchers("/login") .permitAll()
        .anyRequest().authenticated()
        //.antMatchers("/**")
          //  .hasAnyRole("ADMIN", "USER")
        .and()
            .formLogin()
            .loginPage("/login")
            // redirect based on role
            //.defaultSuccessUrl("/home")
            .successHandler(myAuthenticationSuccessHandler())
            .failureUrl("/login?error=true")
            .permitAll()
        .and()
            .logout().deleteCookies("JSESSIONID")
            .logoutSuccessUrl("/login?logout=true")	
            .invalidateHttpSession(true)
            .permitAll()
        // for x509 certificate authentication    
        .and().x509().subjectPrincipalRegex("CN=(.*?)(?:,|$)").userDetailsService(userService)  
        // for remember me
        .and().rememberMe().key("uniqueAndSecret").userDetailsService(userService)//.tokenValiditySeconds(86400)
        .and().addFilterBefore(spnegoAuthenticationProcessingFilter(authenticationManagerBean()), 
        		BasicAuthenticationFilter.class)
        //.and()
            .csrf()
            .disable()
		//.and()
		.headers().frameOptions().disable(); // since we are using thymleaf
	}
	
	/*
	 * @Bean private LogoutSuccessHandler logoutSuccessHandler() { // TODO
	 * Auto-generated method stub return null; }
	 * 
	 * @Bean private AuthenticationFailureHandler authenticationFailureHandler() {
	 * // TODO Auto-generated method stub return null; }
	 */
	
	@Bean
	public AuthenticationSuccessHandler myAuthenticationSuccessHandler() {
		return new MyAuthenticationSuccessHandler();
	}

	@Bean
	public SpnegoAuthenticationProcessingFilter spnegoAuthenticationProcessingFilter(AuthenticationManager authenticationManagerBean)
	{
		System.out.println("in spnegoAuthenticationProcessingFilter");
		SpnegoAuthenticationProcessingFilter filter = new SpnegoAuthenticationProcessingFilter();
		filter.setAuthenticationManager(authenticationManagerBean);
		return filter;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception
	{
		//auth.userDetailsService(userService);
		/*
		 * auth.inMemoryAuthentication()
		 * .withUser("rohit").password("rohit").roles("ADMIN");
		 */
		/*
		 * auth.inMemoryAuthentication() .passwordEncoder(passwordEncoder())
		 * .withUser("user").password(passwordEncoder().encode("123456")).roles("USER")
		 * .and()
		 * .withUser("admin").password(passwordEncoder().encode("123456")).roles("USER",
		 * "ADMIN");
		 */
		auth.authenticationProvider(authProvider());
		// for kerberos authentication
		//.authenticationProvider(kerberosAuthenticationProvider())
		//.authenticationProvider(kerberosServiceAuthenticationProvider());
	}
	
	@Bean
	public KerberosServiceAuthenticationProvider kerberosServiceAuthenticationProvider()
	{
		System.out.println("in kerberosServiceAuthenticationProvider");
		KerberosServiceAuthenticationProvider provider = new KerberosServiceAuthenticationProvider();
		provider.setTicketValidator(sunJaasKerberosTicketValidator());
		provider.setUserDetailsService(userService);
		return provider;
	}

	@Bean
	public SunJaasKerberosTicketValidator sunJaasKerberosTicketValidator()
	{
		System.out.println("in sunJaasKerberosTicketValidator");
		SunJaasKerberosTicketValidator tv = new SunJaasKerberosTicketValidator();
		tv.setServicePrincipal(kerberosPrinciple);
		FileSystemResource fs = new FileSystemResource(kerberosKeytabConfPath);
		System.out.println("keytab exists - "+fs.exists());
		tv.setKeyTabLocation(fs);
		tv.setDebug(true);
		return tv;
	}

	@Bean
	public KerberosAuthenticationProvider kerberosAuthenticationProvider()
	{
		System.out.println("in kerberosAuthenticationProvider");
		KerberosAuthenticationProvider kap =new KerberosAuthenticationProvider();
		SunJaasKerberosClient client = new SunJaasKerberosClient();
		client.setDebug(true);
		kap.setKerberosClient(client);
		kap.setUserDetailsService(userService);
		return kap;
	}

	@Bean
	public DaoAuthenticationProvider authProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userService);
		authProvider.setPasswordEncoder(passwordEncoder());
		return authProvider;
	}

	// this is necessary spring 5 onwards
	@Bean
	public PasswordEncoder passwordEncoder()
	{
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	@Override
	 public AuthenticationManager authenticationManagerBean() throws Exception {
	      return super.authenticationManagerBean();
	}  
	
}
