package in.perpixl.registration.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import in.perpixl.registration.repository.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Long>
{
	Role findByName(String name);
}
