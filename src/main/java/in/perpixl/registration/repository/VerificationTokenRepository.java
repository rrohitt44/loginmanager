package in.perpixl.registration.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import in.perpixl.registration.repository.entity.User;
import in.perpixl.registration.repository.entity.VerificationToken;

public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Long>
{
	VerificationToken findByToken(String token);
	VerificationToken findByUser(User user);
}
