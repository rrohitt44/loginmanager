package in.perpixl.registration.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import in.perpixl.registration.repository.entity.User;

public interface UserRepository extends JpaRepository<User, Long>
{
	public User findByEmail(String email);
	public User findByUsername(String username);
}
