package in.perpixl.registration.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import in.perpixl.registration.repository.entity.Privilege;

public interface PrivilegeRepository extends JpaRepository<Privilege, Long>
{
	Privilege findByName(String name);
}
