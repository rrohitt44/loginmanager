package in.perpixl.registration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import in.perpixl.registration.controller.RegWsController;
import in.perpixl.registration.dto.UserDto;
import in.perpixl.registration.util.Utils;

@SpringBootTest
public class RegWsControllerTest
{
	@Autowired
	RegWsController controller;
	
	@Autowired
	WebApplicationContext context;
	
	MockMvc mockMvc;
	
	@BeforeEach
	public void setUp()
	{
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}
	
	@Test
	public void testSuccess()
	{
		assertThat(controller).isNotNull();
	}
	
	@Test
	@DirtiesContext //reverts the data in the database as if nothing has happened after executing the test case
	public void testUser() throws Exception
	{
		UserDto user = new UserDto();
		user.setEmail("test@test.com");
		user.setUsername("test");
		user.setPassword("test");
		user.setMatchingPassword("test");
		String json = Utils.asJsonString(user);
		mockMvc.perform(
				MockMvcRequestBuilders.post("/register-ws")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE))
		.andDo(print())
		.andExpect(status().isOk())
		;
	}
}
